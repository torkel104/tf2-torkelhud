<?php

class UnknownTokenException extends Exception {
	protected $token;
	protected $lineNumber;

	public function __construct(
			$token = null,
			$lineNumber = null,
			Exception $previous = null)
	{
		$this->token = $token;
		$this->lineNumber = $lineNumber;

		$message = "Unknown token: {$token}";

		parent::__construct($message, null, $previous);
	}

	public function __toString() {
		return __CLASS__ . ": [{$this->lineNumber}]: {$this->message}\n";
	}
}

class UnexpectedTokenException extends Exception {
	protected $token;

	public function __construct(TokenMatch $token = null, Exception $previous = null) {
		$this->token = $token ?? new TokenMatch();

		$name = $token->getName();
		$message = "Unexpected token: {$name}";

		parent::__construct($message, null, $previous);
	}

	public function __toString() {
		$lineNumber = $this->token->getLineNumber();
		return __CLASS__ . ": [{$lineNumber}]: {$this->message}\n";
	}
}

class Token {
	protected $key;
	protected $pattern;
	protected $name;
	protected $ignore;

	public function __construct(
			$key = null,
			string $pattern = null,
			string $name = null,
			bool $ignore = false)
	{
		$this->key = $key;
		$this->pattern = $pattern;
		$this->name = $name;
		$this->ignore = $ignore;
	}

	public function getKey() {
		return $this->key;
	}

	public function getPattern() {
		return $this->pattern;
	}

	public function getName() {
		return $this->name;
	}

	public function getIgnore() {
		return $this->ignore;
	}
}

class TokenMatch {
	protected $token;
	protected $match;
	protected $lineNumber;

	public function __construct(
			Token $token = null,
			string $match = null,
			int $lineNumber = null)
	{
		$this->token = $token ?? new Token();
		$this->match = $match;
		$this->lineNumber = $lineNumber;
	}

	public function getToken() {
		return $this->token;
	}

	public function getMatch() {
		return $this->match;
	}

	public function getLineNumber() {
		return $this->lineNumber;
	}

	public function getKey() {
		return $this->token->getKey();
	}

	public function getPattern() {
		return $this->token->getPattern();
	}

	public function getName() {
		return $this->token->getName();
	}

	public function getIgnore() {
		return $this->token->getIgnore();
	}
}

class Lexer {
	protected $tokens;

	public function __construct(array $tokens = null) {
		$this->tokens = $tokens;
	}

	public function AddToken(Token $t) {
		$this->tokens[] = $t;
	}

	public function AddTokens(array $t) {
		$this->tokens = array_merge($this->tokens, $t);
	}

	protected function GetTokenMatch($string, $lineNumber) {
		foreach ($this->tokens as $token) {
			$matches = null;
			if (preg_match($token->getPattern(), $string, $matches)) {
				return new TokenMatch(
						$token,
						$matches[0],
						$lineNumber
				);
			}
		}

		return null;
	}

	public function Tokenise(string $string) {
		$lines = explode("\n", $string);
		$tokenMatches = [];

		foreach ($lines as $lineNumber => $line) {
			while ($match = $this->GetTokenMatch($line, $lineNumber + 1)) {
				$line = substr($line, strlen($match->getMatch()));
				if (! $match->getIgnore()) {
					$tokenMatches[] = $match;
				}
			}

			if (strlen($line)) {
				throw new UnknownTokenException($line, $lineNumber + 1);
			}
		}

		return $tokenMatches;
	}
}
