"Scheme"
{
	"Fonts"
	{
		"ConsoleText"
		{
			"1"
			{
				"name" "DejaVu Sans Mono"
				"tall" "16"
				"weight" "400"
			}
		}
		"HitMarkerFont1"
		{
			"1"
			{
				"name" "Hit Marker Font"
				"tall" "5"
				"weight" "400"
				"yres" "480 599"
			}
			"2"
			{
				"name" "Hit Marker Font"
				"tall" "7"
				"weight" "400"
				"yres" "600 719"
			}
			"3"
			{
				"name" "Hit Marker Font"
				"tall" "8"
				"weight" "400"
				"yres" "720 899"
			}
			"4"
			{
				"name" "Hit Marker Font"
				"tall" "11"
				"weight" "400"
				"yres" "900 1024"
			}
			"5"
			{
				"name" "Hit Marker Font"
				"tall" "12"
				"weight" "400"
				"yres" "1025 1050"
			}
			"6"
			{
				"name" "Hit Marker Font"
				"tall" "12"
				"weight" "400"
				"yres" "1051 1400"
			}
			"7"
			{
				"name" "Hit Marker Font"
				"tall" "16"
				"weight" "400"
				"yres" "1401 2000"
			}
			"8"
			{
				"name" "Hit Marker Font"
				"tall" "25"
				"weight" "400"
				"yres" "2001 3000"
			}
			"9"
			{
				"name" "Hit Marker Font"
				"tall" "49"
				"weight" "400"
				"yres" "3001 8000"
			}
		}
	}
	"CustomFontFiles"
	{
		"40" "resource/fonts/HitMarkerFont.ttf"
	}
}
