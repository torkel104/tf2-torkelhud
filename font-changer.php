<?php

require __DIR__ . '/libs/lexer.php';
require __DIR__ . '/libs/steam-res-parser.php';

const SOURCE_DIR = __DIR__ . '/default/resource';
const OUTPUT_DIR = __DIR__ . '/resource';

function SortFontWeights(&$fonts) {
	foreach ($fonts as &$font) {
		$weights = (array) $font->weights;
		krsort($weights);
		$font->weights = $weights;
	}
}

function GetReplacementFonts() {
	$json = json_decode(file_get_contents('fonts.json'));
	if (json_last_error()) {
		print "JSON ERROR: " . json_last_error_msg() . PHP_EOL;
		die;
	}
	return $json->fonts;
}

function GetCustomFontFiles() {
	return array_map(function($file) {
		return basename($file);
	}, glob(__DIR__ . '/resource/fonts/*.ttf'));
}

function GetSourceFiles() {
	return glob(SOURCE_DIR . '/*.res');
}

$replacementFonts = GetReplacementFonts();
SortFontWeights($replacementFonts);

foreach (GetSourceFiles() as $sourceFile) {
	$input = file_get_contents($sourceFile);
	$data = SteamResParser::Parse($input);

	$scheme = $data->getScheme(0);
	$fonts = $scheme->getValue('Fonts');

	foreach ($fonts->getValue() as $fontKey => $font) {
		foreach ($font->getValue() as $subFontKey => $subFont) {
			foreach ($subFont->getValue() as $propKey => $prop) {
				if ($prop->getKey() === 'name') {
					$name = $prop->getValue();
					$weightObject = $subFont->getValue('weight');
					$weight = $weightObject ? $weightObject->getValue() : 400;

					foreach ($replacementFonts as $f) {
						foreach($f->replaces as $r) {
							if (! preg_match("/{$r}/i", $name)) {
								continue;
							}

							$name = $f->family;
							foreach ($f->weights as $fw => $fwn) {
								if ($weight >= $fw) {
									$name .= ' ' . $fwn;
									break;
								}
							}
							break 2;
						}
					}

					$prop->setValue(trim($name));
					$subFont->setValue($prop, $propKey);
					$font->setValue($subFont, $subFontKey);
					$fonts->setValue($font, $fontKey);
				}
			}
		}
	}

	$scheme->setValue($fonts, $scheme->indexOf($fonts->getKey()));

	$customFonts = $scheme->getValue('CustomFontFiles');
	if (! $customFonts) {
		$customFonts = new SteamResObject('CustomFontFiles', []);
		$scheme->addValue($customFonts);
	}

	$i = 50;
	foreach (GetCustomFontFiles() as $fontFile) {
		$customFonts->addValue(
			new SteamResObject($i++, "resource/fonts/{$fontFile}")
		);
	}

	$scheme->setValue($customFonts, $scheme->indexOf($customFonts->getKey()));

	$data->setScheme($scheme, 0);

	$output = $data->Serialise();
	file_put_contents(OUTPUT_DIR . '/' . basename($sourceFile), $output);
}
