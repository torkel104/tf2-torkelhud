# TorkelHUD
A barebones Team Fortress 2 HUD. The goal of this HUD is to deviate as little as possible from the default while providing essential fixes and improvements.
# Key Features
- Font weight fixes. TF2 ignores the requested weight when rendering fonts, this is fixed in TorkelHUD by loading and using different fonts for each weight.
- Important information, such as the player's health and ammo, has been moved closer to the center of the screen.
- Min mode. Choose between big and min mode just like in the default HUD.
- Clearer target info
- Fixes the moving chat bug
- Hit markers

# Screenshots
![20180330155207_1](https://user-images.githubusercontent.com/19380776/38144364-56f7a11a-343c-11e8-856c-175ab2f1c0a7.jpg)
![20180330155221_1](https://user-images.githubusercontent.com/19380776/38144417-8075c562-343c-11e8-9bb9-e38701f9e7b8.jpg)
![20180330155227_1](https://user-images.githubusercontent.com/19380776/38144416-7ec233b8-343c-11e8-832b-c4e955f77fa1.jpg)

# Errata
There is a panel visible through the spectator target id
